console.log("Hello World");

// Paramiters anf Arguments

function printName(name = "Chris"){
    console.log(`My name is ${name}`);
}
printName("George");

let sampleVariable = "Edward";
printName(sampleVariable);

// function arguments cannot use if no parameter
function noParams(){
    let params = "No Parameter";
    console.log(params);
}
noParams("with params");

// Checking Divisibility to 8
function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log(`The remainder of ${num} divided by 8 is ${remainder}`);
    let isDivisibleBy8 = remainder === 0;
    
    console.log(`Is ${num} is Divisible by 8?`)
    console.log(isDivisibleBy8);
   
}
checkDivisibilityBy8(16);
checkDivisibilityBy8(17);

// functions as arguments
function argumentFunction(){
    console.log(`This Function was passed as an argument before the mesage was printed.`);
}
function argumentFunction2(){
    console.log(`This function was passed as an argument from the second argument function`);
}

function invokeFunction(argFunction){
    argFunction();
}

invokeFunction(argumentFunction);
invokeFunction(argumentFunction2);

// Multiple arguments
function createFullName(firstName, middleName = "No Middle Name", lastName, prefix){
    console.log(`This is firstName: ${firstName}`);
    console.log(`This is middleName: ${middleName}`);
    console.log(`This is lastName: ${lastName}`);
    console.log(`This is prefix: ${prefix}`);

}
createFullName("Juan", "Dela", "Cruz");

createFullName("Juan", undefined, "Cruz", "Jr.");

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// return statement

function returnFullName(firstName, middleName, lastName){
    console.log(`${firstName} ${middleName} ${lastName}`);
}

returnFullName("Ada", "Love", "Lace");

function returnName(firstName, middleName, lastName){
    return `${firstName} ${middleName} ${lastName}`;
}
console.log(returnName("John", "Doe", "Smith"));

let fullName = returnName("John", "Doe", "Smith");

console.log(fullName);

function printPlayerInfo(userName, level, job){
    console.log(`Username: ${userName}`);
    console.log(`Level: ${level}`);
    console.log(`Job: ${job}`);

    return `${userName} ${level} ${job}`;
}
printPlayerInfo("knight_white", 95, "Paladin");

let user1 = printPlayerInfo("knight_white", 95, "Paladin");

console.log(user1);